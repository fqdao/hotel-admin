 **hotel-admin(酒店管理系统)** 

##### 项目介绍
使用springmvc、springjdbc、shiro、quartz、h-ui构建的一个基础项目框架方便大家学习。在jdbctemplate的基础上封装了一套通用的crud注解，对于单表操作很方便。路过的小伙伴点个star再走


##### 如何启动
1. 需要maven环境支持，执行mvn clean install 下载项目依赖，使用Jdk1.8作为开发环境，采用Jetty启动 jetty:run 命令启动项目 默认端口 8002
1. 配置src/main/resources目录下db.properties文件改成自己的数据源地址
1. 安装redis-windows启动redis服务，用于数据缓存
1. 默认访问地址为http://localhost:8002/sys/login 默认账号都已经在页面配好


##### 内置功能
1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
1. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
1. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
1. 登录日志：系统登录日志记录查询，支持Excel,Pdf,World格式导出
1. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
1. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

##### QQ交流群
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=KBlFDIdTWlbYS3j5EWtyLEq6sjbATnN5&jump_from=webapi">~~335102947~~ </a>🈵️ 、<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=AYY4TTfGFuotB6a9lzLlPdljQO_G9AZF&jump_from=webapi">1090283135</a> 🔥

##### 运行截图:
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162150_76910a2e_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0118/174813_c9af6c29_376915.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162246_69349e0c_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0118/174937_e08dffc7_376915.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162345_f0a371f6_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162404_b74572f9_376915.png"/></td>
    </tr>
     <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162507_feee57b5_376915.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162533_71f2e94a_376915.png"/></td>
    </tr>
     <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162548_dea1c8b6_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1231/162601_f23dccdf_376915.png"/></td>
    </tr>
     <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1230/154150_22acede1_376915.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1230/154243_ccea3040_376915.png"/></td>
    </tr>
</table>
![输入图片说明](http://git.oschina.net/uploads/images/2016/0525/184741_14c8c618_376915.png "在这里输入图片标题")

