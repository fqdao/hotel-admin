﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link href="${ctx }/resources/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resources/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resources/lib/Hui-iconfont/1.0.6/iconfont.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>我的桌面</title>
</head>
<body>
<div class="pd-20" style="padding-top:20px;">
  <p class="f-20 text-success">欢迎使用酒店管理系统 <span class="f-14">v2.3</span></p>
  <p>登录次数：${count }</p>
  <table class="table table-border table-bordered table-bg table-sort">
    <thead>
      <tr>
        <th colspan="7" scope="col">信息统计</th>
      </tr>
      <tr class="text-c">
        <th>浏览器类型</th>
        <th>操作系统</th>
        <th>IP地址</th>
        <th>创建时间</th>
      </tr>
    </thead>
    <tbody>
      <tr class="text-c">
        <td>${d.BROWSER }</td>
        <td>${d.OS }</td>
        <td>${d.IP }</td>
        <td>${d.tim }</td>
      </tr>
    </tbody>
  </table>
</div>
<script type="text/javascript" src="${ctx }/resources/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/H-ui.js"></script>
<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
</body>
<script>
$(function(){
  $.fn.dtconfig = {
    serverSide: true,
    autoWidth: false,//是否自动计算表格各列宽度
    searching: false,//是否显示搜索框
    lengthChange: false,//是否显示每页大小的下拉框
    columns: [{
      data : "browser"
    },{
      data : "os"
    },{
      data : "ip"
    },{
      data : "createDate",
      render: function ( data, type, full, meta ) {
        return timeStamp2String(data);
      }
    }],
    language: {
      lengthMenu: "每页显示 _MENU_记录",
      zeroRecords: "没有匹配的数据",
      info: "第_PAGE_页/共 _PAGES_页 ( 共\_TOTAL\_条记录 )",
      infoEmpty: "没有符合条件的记录",
      search: "查找",
      infoFiltered: "(从 _MAX_条记录中过滤)",
      paginate: { "first": "首页 ", "last": "末页", "next": "下一页", "previous": "上一页" }
    },
  }

  //配置服务端请求（必须）
  $.fn.dtconfig.ajax = {
    url: '/sys/data',//请求地址
    type: 'get',// 异步请求方式
    data:{}
  };
//在Jquery里格式化Date日期时间数据
  function timeStamp2String(time){
    var datetime = new Date();
    datetime.setTime(time);
    var year = datetime.getFullYear();
    var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
    var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
    var hour = datetime.getHours()< 10 ? "0" + datetime.getHours() : datetime.getHours();
    var minute = datetime.getMinutes()< 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
    var second = datetime.getSeconds()< 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
    return year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;
  }
  $('.table-sort').DataTable($.fn.dtconfig);
  $('.table-sort tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      $(this).addClass('selected');
    }
  });
});
</script>
</html>