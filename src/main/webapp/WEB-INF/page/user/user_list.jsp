﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link href="${ctx }/resources/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resources/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/resources/lib/Hui-iconfont/1.0.6/iconfont.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>用户管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span> 用户管理 <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="pd-20">
	<div class="cl pd-5 bg-1 bk-gray"> 
		<span class="l"> <a class="btn btn-success radius size-S" href="javascript:;" onclick="add_user('添加用户','${ctx}/user/toAdd','700','300')" ><i class="Hui-iconfont">&#xe600;</i> 添加用户</a> </span>  </div>
	<table class="table table-border table-bordered table-hover table-bg table-sort">
		<thead>
			<tr>
				<th scope="col" colspan="9">用户管理</th>
			</tr>
			<tr class="text-c">
				<th width="40">ID</th>
				<th width="50">昵称</th>
				<th width="70">邮箱</th>
				<th width="70">性别</th>
				<th width="70">登录次数</th>
				<th width="70">上次登录时间</th>
				<th width="70">描述</th>
				<th width="70">创建时间</th>
				<th width="70">操作</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
</div>

<script type="text/javascript" src="${ctx }/resources/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="${ctx }/resources/lib/layer/2.1/layer.js"></script> 
<script type="text/javascript" src="${ctx }/resources/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/H-ui.js"></script> 
<script type="text/javascript" src="${ctx }/resources/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctx }/resources/lib/Validform/5.3.2/Validform.min.js"></script>
<script type="text/javascript">
/*添加用户*/
function add_user(title,url,w,h){
	layer_show(title,url,w,h);
}
function change_role(url){
	url = "${ctx }/user/toRole/" + url;
	layer_show("设置角色",url,700,400);
}
$(function(){
//基本配置
$.fn.dtconfig = {
	serverSide: true,
	autoWidth: false,//是否自动计算表格各列宽度
	searching: false,//是否显示搜索框
	lengthChange: false,//是否显示每页大小的下拉框
	columns: [{
		data : "id"
	},{
		data : "name"
	},{
		data : "email"
	},{
		data : "gender",
		render: function ( data, type, full, meta ) {
			return  data;
		}
	},{
		data : "loginCount"
	},{
		data : "lastVisit",
		render: function ( data, type, full, meta ) {
			return timeStamp2String(data);
		}
	},{
		data : "description"
	},{
		data : "createDate",
		render: function ( data, type, full, meta ) {
			return timeStamp2String(data);
		}
	},{
		data : "id",
		render: function (data, type, full, meta ) {
			var btn = "";
		    btn += '<input class="btn btn-success radius size-MINI" onclick="change_role('+data+')" type="button" value="查看角色">';
		    btn	+= '&nbsp;&nbsp;&nbsp;&nbsp;';
			btn += '<input class="btn btn-success radius size-MINI" onclick="deleteRole('+data+')" type="button" value="删除">';
			return btn;
		}
	}],
	language: {
		lengthMenu: "每页显示 _MENU_记录",
		zeroRecords: "没有匹配的数据",
		info: "第_PAGE_页/共 _PAGES_页 ( 共\_TOTAL\_条记录 )",
		infoEmpty: "没有符合条件的记录",
		search: "查找",
		infoFiltered: "(从 _MAX_条记录中过滤)",
		paginate: { "first": "首页 ", "last": "末页", "next": "下一页", "previous": "上一页" }
	},
}

	//配置服务端请求（必须）
	$.fn.dtconfig.ajax = {
		url: '/user/data',//请求地址
		type: 'get',// 异步请求方式
		data:{}
	};
//在Jquery里格式化Date日期时间数据
	function timeStamp2String(time){
		var datetime = new Date();
		datetime.setTime(time);
		var year = datetime.getFullYear();
		var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
		var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
		var hour = datetime.getHours()< 10 ? "0" + datetime.getHours() : datetime.getHours();
		var minute = datetime.getMinutes()< 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
		var second = datetime.getSeconds()< 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
		return year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;
	}
	$('.table-sort').DataTable($.fn.dtconfig);
	$('.table-sort tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			$(this).addClass('selected');
		}
	});


});
function deleteRole(id) {
	$.post("${ctx }/user/delete/" + id,"", function (data) {
        console.log(data)
		if(data.code==200){
				layer.msg(data.msg,{icon: 1,time:1000},function(){
					location.reload();
					layer_close();
				});
		} else {
			layer.msg(data.msg,{icon: 2,time:1000},function(){});
		}
	});
}
</script>
</body>
</html>