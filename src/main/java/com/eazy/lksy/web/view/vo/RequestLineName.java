package com.eazy.lksy.web.view.vo;

import lombok.Data;

@Data
public class RequestLineName {

    String cityid;

    String begintime;

    String endtime;
}
