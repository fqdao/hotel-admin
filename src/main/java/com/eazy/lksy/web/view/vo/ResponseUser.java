package com.eazy.lksy.web.view.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ResponseUser {

    private Integer id;

    private String name;

    private String gender;

    private String email;

    private Date lastVisit;

    private Date createDate;

    private String description;

    private String loginCount;
}
