package com.eazy.lksy.web.view.vo;

import com.eazy.lksy.web.model.User;
import lombok.Data;

import java.util.List;

@Data
public class RequestUser {

    private User user;

    private List<String> roleId;
}
