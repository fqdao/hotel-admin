package com.eazy.lksy.web.view;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 封装分页对象 
 **/
@Data
public class PageList<E> {
    private int recordsTotal ;
    private int recordsFiltered ;
    private int draw;
    private List<E> data=new ArrayList<E>();


}