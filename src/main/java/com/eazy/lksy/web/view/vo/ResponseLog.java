package com.eazy.lksy.web.view.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ResponseLog {

    private Integer id;

    private String os;

    private String browser;

    private String ip;

    private Date createDate;

}
