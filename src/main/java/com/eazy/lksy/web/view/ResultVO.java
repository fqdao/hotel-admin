package com.eazy.lksy.web.view;


public class ResultVO {


    public ResultVO(boolean ok) {
        this.ok = ok;
    }

    public ResultVO() {
    }

    private String code;

    private boolean ok;

    private String msg;

    private String url;

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static ResultVO success(String code, String message) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMsg(message);
        return resultVO;
    }


    public static ResultVO success(String code, String message,String data) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMsg(message);
        resultVO.setData(data);
        return resultVO;
    }


    public static ResultVO success() {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode("200");
        return resultVO;
    }

    public static ResultVO errorMessage(String message) {
        ResultVO resultVO = new ResultVO();
        resultVO.setMsg("400");
        resultVO.setMsg(message);
        return resultVO;
    }
}
