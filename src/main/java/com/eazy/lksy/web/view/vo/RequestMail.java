package com.eazy.lksy.web.view.vo;

import lombok.Data;

@Data
public class RequestMail {

    private String title;

    private String content;

    private String toAddress;

    private String type;

    private String path;
}
