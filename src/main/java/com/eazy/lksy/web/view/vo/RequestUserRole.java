package com.eazy.lksy.web.view.vo;

import lombok.Data;

import java.util.List;

@Data
public class RequestUserRole {

    private  String user_id;

    private  List<String> roleId;
}
