package com.eazy.lksy.web.view.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ResponseArea {

    String name;
    String cityId;

    Date createTime;
}
