package com.eazy.lksy.web.view.vo;

import lombok.Data;

@Data
public class RequestCity {

    private String name;

    private String city_id;
}
