package com.eazy.lksy.web.controller;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.Line;

import com.eazy.lksy.web.model.City;
import com.eazy.lksy.web.model.LineName;
import com.eazy.lksy.web.utils.Bundle;
import com.eazy.lksy.web.view.vo.RequestLineName;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.CityService;
import com.eazy.lksy.web.service.LineService;


@Controller
@RequestMapping(value = "line")
public class LineController extends BaseController {

	@Autowired
	private CityService cityService;
	@Autowired
	private LineService lineService;

	/**
	 * 线路查询列表
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView list(RequestLineName request) {
		ModelAndView view = new ModelAndView("city/line/line_list");
		Map<String,String> map = getFormPage();
		view.addObject("cityData", cityService.select());
		view.addObject("lineData", lineService.selectLineName(request));
		return view;
	}
	
	
	/**
	 * 添加线路
	 */
	@RequestMapping(value = "/addLine", method = RequestMethod.POST)
	public String addLine(LineName lineName) {
		if(lineService.isExists(lineName.getCityId(), lineName.getName())) {
			lineService.insert(lineName);
		}
		return "redirect:/line/list";
	}
	
	/**
	 * 修改线路
	 */
	@RequestMapping(value = "/updateLine", method = RequestMethod.POST)
	public String updateLine(LineName lineName) {
		lineService.update(lineName);
		return "redirect:/line/list";
	}
	
	/**
	 * 删除线路
	 */
	@RequiresPermissions("sys:line:delete")
	@RequestMapping(value = "/deleteLine/{id}", method = RequestMethod.GET)
	public String deleteLine(@PathVariable String id) {
		lineService.deleteById(id);
		return "redirect:/line/list";
	}
	
	/**
	 * 跳转到添加线路
	 */
	@RequiresPermissions("sys:line:add")
	@RequestMapping(value = "/toAddLine", method = RequestMethod.GET)
	public ModelAndView toAddLine() {
		ModelAndView view = new ModelAndView("city/line/line_add");
		view.addObject("cityData", cityService.select());
		return view;
	}
	
	/**
	 * 跳转到修改线路页面
	 */
	@RequestMapping(value = "/toUpdateLine/{id}", method = RequestMethod.GET)
	public ModelAndView toUpdateLine(@PathVariable String id) {
		ModelAndView view = new ModelAndView("city/line/line_update");

		LineName lineModel = lineService.selectById(id);
		City cityModel = cityService.selectById(lineModel.getCityId());
		List<City> cityArray = cityService.select();
		ListIterator<City> data = cityArray.listIterator();
		synchronized(data) {
			while(data.hasNext()) {
				City d = data.next();
				if(cityModel.getId() == d.getId()) {
					data.remove();
				}
			}
		}
		view.addObject("cityData", cityArray);
		view.addObject("line_name",lineModel.getName());
		view.addObject("id", lineModel.getId());
		return view;
	}
}
