package com.eazy.lksy.web.controller;


import com.eazy.lksy.web.model.Area;
import com.eazy.lksy.web.view.ResultVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.AreaService;

/**
 * 区域管理
 */
@Controller
@RequestMapping("/area")
public class AreaController extends BaseController {

	
	@Autowired
	private AreaService areaService;
	
	/**
	 * 区域查询
	 */
	@RequestMapping(value = "list/{id}" , method = RequestMethod.GET)
	public String select(@PathVariable String id,Model model) {
		model.addAttribute("city_id",id);
		model.addAttribute("data",areaService.list(id));
		return "city/area/area_list";
	}
	

	/**
	 * 区域添加
	 */
	@RequestMapping(value = "addArea" , method = RequestMethod.POST)
	@ResponseBody
	public ResultVO addArea(Area area) {
		areaService.insert(area);
		return ResultVO.success();
	}

	/**
	 * 区域修改
	 */
	@RequestMapping(value = "updateArea" , method = RequestMethod.POST)
	@ResponseBody
	public ResultVO updateArea(Area area) {
		areaService.update(area);
		return ResultVO.success();
	}

	/**
	 * 区域删除
	 */
	@RequiresPermissions("sys:area:delete")
	@RequestMapping(value = "deleteArea/{id}" , method = RequestMethod.GET)
	@ResponseBody
	public ResultVO deleteArea(@PathVariable String id) {
		areaService.deleteById(id);
		return ResultVO.success();
	}

	/**
	 * 跳转到区域添加
	 */
	@RequestMapping(value = "toAddarea/{id}" , method = RequestMethod.GET)
	public ModelAndView toAddarea(@PathVariable String id) {
		return new ModelAndView("city/area/area_add","city_id",id);
	}

	/**
	 * 跳转到区域修改
	 */
	@RequestMapping(value = "toUpdatearea/{id}" , method = RequestMethod.GET)
	public ModelAndView toUpdatearea(@PathVariable String id) {
		Area result = areaService.selectById(id);
		return  new ModelAndView("city/area/area_update","data", result);
	}
}
