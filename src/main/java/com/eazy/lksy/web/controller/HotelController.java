package com.eazy.lksy.web.controller;


import javax.servlet.http.HttpServletRequest;

import com.eazy.lksy.web.model.Hotel;
import com.eazy.lksy.web.view.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.HotelService;

/**
 * @author jzx
 * @date 2016/3/30
 * @desc 酒店管理
 */
@Controller
@RequestMapping(value = "hotel")
public class  HotelController  extends BaseController {

	/**
	 * 2016.5.24 增加酒店添加功能
	 */
	@Autowired
	private HotelService hotelService;
	/**
	 * 酒店查询列表
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView list() {
		ModelAndView view = new ModelAndView("hotel/hotel_list"); 
		view.addObject("data", hotelService.select());
		return view;
	}
	
	/**
	 * 酒店添加功能
	 */
	@RequestMapping(value = "/addHotel", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResultVO addHotel(HttpServletRequest request, Hotel hotel) {
		String [] title = getParaValues("title");
		String [] success = getParaValues("success");
		
		if(title == null) {
			return ResultVO.errorMessage("酒店图片不能为空");
		}
		if(success == null) {
			return ResultVO.errorMessage("请先批量上传图片，然后在提交！！！！");
		}
		
		String photo_name = title[0]; 
		photo_name = photo_name.substring(0, photo_name.lastIndexOf("."));
		hotel.setImg(photo_name);

		hotelService.insert(hotel);

		return ResultVO.success();
	}
	
	/**
	 * 跳转到酒店添加页面
	 */
	@RequestMapping(value = "/toAddHotel", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView toAddHotel() {
		return new ModelAndView("/hotel/hotel-add");
	}
	
	/**
	 * 跳转到房间图片
	 */
	@RequestMapping(value = "/toRoomImage/{id}", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView toRoomImage(@PathVariable String id) {
		ModelAndView view = new ModelAndView("hotel/room_image"); 
		view.addObject("data", hotelService.getRoomImage(id));
		return view;
	}
	
	
}
