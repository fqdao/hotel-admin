package com.eazy.lksy.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eazy.lksy.web.model.Permission;
import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.ResponseLog;
import com.eazy.lksy.web.view.vo.ResponseUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.model.User;
import com.eazy.lksy.web.redis.Redis;
import com.eazy.lksy.web.service.LogService;
import com.eazy.lksy.web.service.PermissionService;
import com.eazy.lksy.web.service.UserService;
import com.eazy.lksy.web.utils.DataUtil;
import com.eazy.lksy.web.utils.MD5;
import com.eazy.lksy.web.utils.StrKit;

/**
 * 登录管理
 */
@Controller
@RequestMapping("/sys")
public class LoginController extends BaseController {

	@Resource
	private JmsTemplate jmsTemplate;
	@Autowired
	private LogService logService;
	@Autowired
	private UserService userService;
	@Autowired
	private PermissionService permissionService;

	/**
	 * 用户登录
	 */
	@RequestMapping(value = "/login", method = { RequestMethod.POST,RequestMethod.GET})
	public String login(HttpServletRequest request,RedirectAttributes redirectAttributes) {
		Map<String, String>  map = getPageData(request);
		//抛弃使用session，存放到redis中
		String code = Redis.get("code");

		try {
			if (null != code && !code.equalsIgnoreCase(map.get("code"))) {
				redirectAttributes.addFlashAttribute("msg", "验证码输入错误");
			}
			UsernamePasswordToken token = new UsernamePasswordToken(map.get("name"),map.get("pwd"));
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.login(token);
			if (currentUser.isAuthenticated()) {
				userService.updateVisit(getCurrentUser().getId());
				return "redirect:index";
			}
		} catch (UnknownAccountException ex) {
			redirectAttributes.addFlashAttribute("msg", "用户不存在!");
		} catch (IncorrectCredentialsException ex) {
			redirectAttributes.addFlashAttribute("msg", "密码匹配错误!");
		}  catch (ExcessiveAttemptsException e) {
			//error = "登录失败多次，账户锁定10分钟";
		} catch (AuthenticationException ex) {
			redirectAttributes.addFlashAttribute("msg", "用户名或者密码输入错误!!!");
		}
		return "redirect:error";
	}

	/**
	 * 用户登出
	 */
	@RequestMapping(value = "/logout", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView logout() {
		ModelAndView andView = new ModelAndView("login/login");
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return andView;
	}
	
	/**
	 * 修改登录密码
	 */
	@RequestMapping(value = "/forgetpwd" , method = RequestMethod.POST)
	public String updatePwd(User user) {
		userService.updateUser(user);
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/sys/login";
	}
	
	/**
	 * 跳转到欢迎页
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView welcome() {
		ModelAndView model = new ModelAndView("index/welcome");
		model.addObject("count",logService.count());
		return model;
	}

	/**
	 * 异步获取数据
	 */
	@RequestMapping(value = "data", method = RequestMethod.GET)
	@ResponseBody
	public PageList<ResponseLog> data(Integer start, Integer length, Integer draw) {
		PageList<ResponseLog> list = logService.getLogPage(start, length);
		list.setDraw(draw);
		return list;
	}


	/**
	 * 跳转到首页
	 */
	@RequiresAuthentication
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(ModelMap map) {
		String value = getCurrentUser().getId().toString();
		List<Permission> result = permissionService.permissionList(value);
		return new ModelAndView("index/index","data",result);
	}

	/**
	 * 跳转到登录失败页面
	 */
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public ModelAndView error(ModelMap model) {
		return new ModelAndView("login/login",model);
	}
	

}
