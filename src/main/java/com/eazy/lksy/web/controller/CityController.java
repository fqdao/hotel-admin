package com.eazy.lksy.web.controller;

import com.eazy.lksy.web.model.City;
import com.eazy.lksy.web.view.ResultVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.CityService;

/**
 * 城市管理
 */
@Controller
@RequestMapping("/city")
public class CityController extends BaseController {

	@Autowired
	private CityService cityService;
	
	/**
	 * 城市查询
	 */
	@RequestMapping(value = "list" , method = RequestMethod.GET)
	public ModelAndView select() {
		return new ModelAndView("city/city_list","data",cityService.select());
	}
	
	/**
	 * 城市添加功能
	 */
	@RequestMapping(value = "addcity" , method = RequestMethod.POST)
	@ResponseBody
	public ResultVO addCity(City city) {
		cityService.insert(city);
		return ResultVO.success();
	}

	/**
	 * 城市修改功能
	 */
	@RequestMapping(value = "updatecity" , method = RequestMethod.POST)
	@ResponseBody
	public ResultVO updCity(City city) {
		cityService.update(city);
		return ResultVO.success();
	}
	
	/**
	 * 城市删除
	 */
	@RequiresPermissions("sys:city:delete")
	@RequestMapping(value = "deletecity/{id}" , method = RequestMethod.GET)
	@ResponseBody
	public ResultVO delCity(@PathVariable String id) {
		cityService.deleteById(id);
		return ResultVO.success();
	}
	
	/**
	 * 跳转到城市添加页面
	 */
	@RequestMapping(value = "toAddcity" , method = RequestMethod.GET)
	public String toAddcity() {
		return "city/city_add";
	}
	
	/**
	 * 跳转到城市修改页面
	 */
	@RequestMapping(value = "toUpdatecity/{id}" , method = RequestMethod.GET)
	public ModelAndView toUpdatecity(@PathVariable String id) {
		ModelAndView modelAndView = new ModelAndView("city/city_update");
		modelAndView.addObject(cityService.selectById(id));
		return modelAndView;
	}
	
}
