package com.eazy.lksy.web.controller;  


import javax.servlet.http.HttpServletRequest;

import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.ResultVO;
import com.eazy.lksy.web.view.vo.RequestUser;
import com.eazy.lksy.web.view.vo.RequestUserRole;
import com.eazy.lksy.web.view.vo.ResponseUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.RoleService;
import com.eazy.lksy.web.service.UserService;
  

/**
 * 用户管理
 */
@Controller  
@RequestMapping("/user")  
public class UserController extends BaseController {  
  
    @Autowired  
    private UserService userService;  
    @Autowired
    private RoleService roleService;
    
    
    /**
     * 查询用户列表
     */
    @RequestMapping(value = "list" , method = RequestMethod.GET)  
    public ModelAndView list(Model model) {
        return new ModelAndView("user/user_list");  
    }

    /**
     * 异步获取数据
     */
    @RequestMapping(value = "data", method = RequestMethod.GET)
    @ResponseBody
    public PageList<ResponseUser> data(Integer start, Integer length, Integer draw) {
        PageList<ResponseUser> list = userService.getUserPage(start, length);
        list.setDraw(draw);
        return list;
    }


    /**
     * 添加用户角色
     */
    @RequestMapping(value = "addUserRole" , method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResultVO addUserRole(RequestUserRole request) {
        roleService.addUserRole(request);
        return ResultVO.success();
    }  
    
    
    /**
     * 添加用户
     */
    @RequestMapping(value = "addUser", method = RequestMethod.POST)
    @ResponseBody
    public ResultVO addUser(RequestUser request) {
    	userService.addUserRole(request);
    	return ResultVO.success();
    }
    
    /**
     * 删除用户 
     */
    @RequiresPermissions("sys:user:delete")
    @RequestMapping(value = "delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ResultVO deleteUser(@PathVariable String id) {
        roleService.deleteUser(id);
    	return ResultVO.success("200", "删除用户信息成功");
    }
    
    /**
     * 跳转到设置角色页面
     */
    @RequiresPermissions("sys:user:toRole")
    @RequestMapping(value = "toRole/{id}", method = RequestMethod.GET)
    public ModelAndView toRole(@PathVariable String id) {
    	ModelMap map = new ModelMap("all",roleService.toRoles(id));
    	map.put("id", id);
    	return new ModelAndView("user/user_role",map);
    }
    
    
    /**
     * 跳转到修改用户页面并获取用户ID
     */
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public ModelAndView toUpdateUser(@RequestParam String id) {
    	return new ModelAndView("/user/update","user",getCurrentUser());
    }
    
    /**
     * 跳转到添加用户页面
     */
    @RequiresPermissions("sys:user:add")
    @RequestMapping(value = "toAdd", method = RequestMethod.GET)
    public ModelAndView toAddUser(HttpServletRequest request) {
    	return new ModelAndView("user/user_add","role",roleService.select());
    }


    /**
     * 跳转到修改密码页面
     */
    @RequestMapping(value = "/forget" , method = RequestMethod.GET)
    public ModelAndView toForget() {
        return new ModelAndView("admin/admin_pwd","user",userService.selectById(getCurrentUser().getId()));
    }


}