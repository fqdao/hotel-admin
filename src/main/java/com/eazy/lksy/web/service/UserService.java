package com.eazy.lksy.web.service;  
  
import java.util.List;  
import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonDao;
import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.User;
import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.RequestUser;


public interface UserService extends CommonService<User>  {


    public User getUserByName(String userName);

    public void updateVisit(Integer userId);
    
    public void addUserRole(String cust_id,String role_id);
    
    public void addUserRole(RequestUser request);

    PageList getUserPage(Integer start, Integer length);

    void updateUser(User user);
}