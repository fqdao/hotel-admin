package com.eazy.lksy.web.service.impl;  
  
import java.util.List;  
import java.util.Map;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.eazy.lksy.web.exception.ServiceException;
import com.eazy.lksy.web.utils.MD5;
import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.RequestUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.dao.UserDao;
import com.eazy.lksy.web.model.User;
import com.eazy.lksy.web.service.UserService;  
  
  
@Service  
public class UserServiceImpl extends CommonServiceImpl<User> implements UserService {
  
    @Autowired  
    private UserDao userDao;  

	@Override
	public User getUserByName(String userName) {
		return userDao.getUserByName(userName);
	}

	@Override
	public void updateVisit(Integer userId) {
		userDao.updateVisit(userId);
	}


	@Override
	public PageList getUserPage(Integer start, Integer length) {
		return userDao.getUserPage(start,length);
	}

	@Override
	public void updateUser(User user) {
		user.setPassword(MD5.encodeString(user.getPassword()));
		update(user);
	}

	@Override
	public void addUserRole(String cust_id, String role_id) {
		userDao.addUserRole(cust_id, role_id);
	}

	@Override
	public void addUserRole(RequestUser request) {
		if(CollUtil.isEmpty(request.getRoleId())) {
			throw new ServiceException("请至少选择一个用户身份信息");
		}
		User user = userDao.getUserByName(request.getUser().getName());
		if(ObjectUtil.isNotEmpty(user)) {
			throw new ServiceException("已经存在此用户了");
		}
		request.getUser().setPassword(MD5.encodeString(request.getUser().getPassword()));
		request.getUser().setId(NumberUtil.parseInt(RandomUtil.randomNumbers(7)));
		insert(request.getUser());

		request.getRoleId().stream().forEach((roleId) ->{
			userDao.addUserRole(request.getUser().getId().toString(), roleId);
		});
	}


}  