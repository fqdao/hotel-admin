package com.eazy.lksy.web.service.impl;

import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.core.dao.CommonDaoImpl;
import com.eazy.lksy.web.dao.HotelDao;
import com.eazy.lksy.web.service.HotelService;

import java.util.List;
import java.util.Map;

@Service
public class HotelServiceImpl extends CommonServiceImpl<Hotel> implements HotelService {

	@Autowired
	private HotelDao hotelDao;


	@Override
	public List<Map<String, Object>> getRoomImage(String hotelId) {
		return hotelDao.getRoomImage(hotelId);
	}
}
