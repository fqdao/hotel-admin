package com.eazy.lksy.web.service;

import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonDao;
import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.LineName;
import com.eazy.lksy.web.view.vo.RequestLineName;

public interface LineService extends CommonService<LineName> {
	
	public void deleteLine(String line_id);
	
	public boolean isExists(String city_id,String line_name);

	public void updateLineName(Map<String,String> map);
	
	public List<Map<String, Object>> selectLine(String city_id);

	public List<LineName> selectLineName(RequestLineName request);
	
}