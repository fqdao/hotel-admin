package com.eazy.lksy.web.service.impl;


import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.dao.CityDao;
import com.eazy.lksy.web.service.CityService;

@Service
public class CityServiceImpl extends CommonServiceImpl<City> implements CityService {

	@Autowired
	private CityDao cityDao;

}
