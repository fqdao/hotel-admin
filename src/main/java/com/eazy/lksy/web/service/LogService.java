package com.eazy.lksy.web.service;

import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.Log;
import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.ResponseLog;

public interface LogService extends CommonService<Log> {

	public void save(Log log);
	public List<Map<String,Object>> selectLog();
	public List<Map<String, Object>> selectLog(String beginTime,String endTime);

	PageList getAllPage(Integer pageNo, Integer pageSize);

	PageList<ResponseLog> getLogPage(Integer start, Integer length);
}
