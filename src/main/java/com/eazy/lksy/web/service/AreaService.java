package com.eazy.lksy.web.service;


import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.Area;

public interface AreaService extends CommonService<Area> {

	List<Map<String, Object>> list(String city_id);
}
