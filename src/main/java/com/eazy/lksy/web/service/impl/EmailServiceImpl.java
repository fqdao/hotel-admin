package com.eazy.lksy.web.service.impl;

import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.dao.EmailDao;
import com.eazy.lksy.web.service.EmailService;

@Service
public class EmailServiceImpl extends CommonServiceImpl<Mail> implements EmailService {

	@Autowired
	private EmailDao emailDao;

}
