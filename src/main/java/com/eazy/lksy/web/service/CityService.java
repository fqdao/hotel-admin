package com.eazy.lksy.web.service;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.City;

public interface CityService extends CommonService<City> {

}
