package com.eazy.lksy.web.service.impl;

import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.model.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.dao.AreaDao;
import com.eazy.lksy.web.service.AreaService;

@Service
public class AreaServiceImpl extends CommonServiceImpl<Area> implements AreaService {

	@Autowired
	private AreaDao areaDao; 
	
	@Override
	public List<Map<String, Object>> list(String cityId) {
		return areaDao.list(cityId);
	}



}
