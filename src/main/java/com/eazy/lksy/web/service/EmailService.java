package com.eazy.lksy.web.service;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.Mail;


public interface EmailService extends CommonService<Mail> {

}
