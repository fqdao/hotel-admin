package com.eazy.lksy.web.service;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.Role;
import com.eazy.lksy.web.view.vo.RequestUserRole;

import java.util.List;
import java.util.Map;

public interface RoleService extends CommonService<Role> {

	public void addRolePermission(String role_id,String permission_id);
	
	public String addRole(Map<String,String> map);
	
	public Map<String,Object> getRole(String role_id);
	
    public void deleteUserRole(String user_id);
    
    public void updateRole(Map<String,String> map);
	
	public List<Map<String,Object>> getRoles(String cust_id);
	
	public List<Map<String, Object>> getRolePermissions(String id);

	public void addUserRole(RequestUserRole request);

	public void deleteUser(String id);

	public List<Role> toRoles(String roleId);
}
