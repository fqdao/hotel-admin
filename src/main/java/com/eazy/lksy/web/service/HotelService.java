package com.eazy.lksy.web.service;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.model.Hotel;

import java.util.List;
import java.util.Map;

public interface HotelService extends CommonService<Hotel> {

    public List<Map<String,Object>> getRoomImage(String hotelId);

}
