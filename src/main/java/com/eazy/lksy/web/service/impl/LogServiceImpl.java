package com.eazy.lksy.web.service.impl;

import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.core.dao.CommonService;
import com.eazy.lksy.web.core.dao.CommonServiceImpl;
import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.ResponseLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eazy.lksy.web.dao.LogDao;
import com.eazy.lksy.web.model.Log;
import com.eazy.lksy.web.service.LogService;
@Service
public class LogServiceImpl extends CommonServiceImpl<Log> implements LogService {

	/**
	 * 2016/3/14 19:37
	 * 新添加功能
	 */
	@Autowired 
	private LogDao logDao;

	@Override
	public List<Map<String, Object>> selectLog() {
		return logDao.selectLog();
	}

	@Override
	public void save(Log log) {
		logDao.save(log);
	}

	@Override
	public List<Map<String, Object>> selectLog(String beginTime, String endTime) {
		return logDao.selectLog(beginTime, endTime);
	}

	@Override
	public PageList getAllPage(Integer pageNo,Integer pageSize) {
		return logDao.getAllPage(pageNo,pageSize);
	}

	@Override
	public PageList<ResponseLog> getLogPage(Integer start, Integer length) {
		return logDao.getAllPage(start,length);
	}


}
