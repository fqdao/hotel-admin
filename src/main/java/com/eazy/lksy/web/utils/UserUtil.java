package com.eazy.lksy.web.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.eazy.lksy.web.model.User;

public class UserUtil {
	
	
	/**
	 * 获取当前用户session
	 * @return session
	 */
	public static Session getSession(){
		Session session =SecurityUtils.getSubject().getSession();
		return session;
	}
	
	/**
	 * 获取当前用户httpsession
	 * @return httpsession
	 */
	public static Session getHttpSession(){
		Session session =SecurityUtils.getSubject().getSession();
		return session;
	}
	
	/**
	 * 获取当前用户对象
	 * @return user
	 */
	public static User getCurrentUser(){
		Integer userId = (Integer) SecurityUtils.getSubject().getPrincipal();
		User user = new User();
		user.setId(userId);
		return user;
	}
}
 