package com.eazy.lksy.web.exception;


import com.eazy.lksy.web.view.ResultVO;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(value = ServiceException.class)
    @ResponseBody
    public ResultVO handleServiceException(ServiceException e) {
        e.printStackTrace();
        ResultVO resultVO = new ResultVO();
        resultVO.setData(e.getMessage());
        return ResultVO.errorMessage(e.getMessage());
    }

}
