package com.eazy.lksy.web.exception;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 97888542202678197L;

	public ServiceException(String message) {
		super(message);
	}
}
