package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Column;
import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Table(name = "user")
@Data
public class User  implements Serializable {

	@Id(value = "random")
	private Integer id;
	private String name;
	private String password;
	@com.eazy.lksy.core.annotation.Date(type = "format" , format = "yyyy-MM-dd")
	@Column(field = "create_date")
	private Date createDate;
	private String state="0";
	private String email;

}
