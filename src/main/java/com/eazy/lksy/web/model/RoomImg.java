package com.eazy.lksy.web.model;

import java.io.Serializable;

public class RoomImg implements Serializable {

	public RoomImg(String image, String roomId) {
		super();
		this.image = image;
		this.roomId = roomId;
	}

	private static final long serialVersionUID = -7944460832338251674L;
	
	private String image;
	private String roomId;
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

}
