package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Column;
import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Ignore;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

import java.util.Date;

@Data
@Table(name = "line_name")
public class LineName {

    @Id
    private Integer id;

    private String name;

    @Column(field = "city_id")
    private String cityId;
    @Ignore
    private String cityName;

    @Column(field = "create_time")
    @com.eazy.lksy.core.annotation.Date(type = "format" , format = "yyyy-MM-dd")
    private Date createTime;

    private String status="0";
}
