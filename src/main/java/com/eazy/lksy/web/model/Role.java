package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Ignore;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

@Table(name = "role")
@Data
public class Role {

    @Id
    private Integer id;
    private String name;
    private String roleCode;
    private String description;
    private Integer sort;
    @Ignore
    private Integer point;

}
