package com.eazy.lksy.web.model;

import java.io.Serializable;

import com.eazy.lksy.core.annotation.Column;
import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;


@Data
@Table(name = "hotel")
public class Hotel implements Serializable {

	private static final long serialVersionUID = 4984468442314823293L;

	@Id
	private String id;
	@Column(field = "hotel_name")
	private String hotelName;
	private String tel;
	private String address;
	@Column(field = "city_id")
	private String cityId;
	private String img;
	private String star;
	private String about;
	private String log;
	private String lat;
	private String des;

	
	
}
