package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Column;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

@Table(name = "permission")
@Data
public class Permission {

    private  Integer id;
    @Column(field = "pid")
    private Integer pid;
    private String name;
    private String type;
    private Integer sort;
    private String url;
    private String icon;
}
