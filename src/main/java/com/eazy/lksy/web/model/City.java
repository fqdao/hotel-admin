package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

@Data
@Table(name = "city")
public class City {

    @Id(value = "auto")
    private Integer id;

    private String name;

    private String description;

    private String status = "0";
}
