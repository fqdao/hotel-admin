package com.eazy.lksy.web.model;

import com.eazy.lksy.core.annotation.Id;
import com.eazy.lksy.core.annotation.Table;
import lombok.Data;

@Data
@Table(name = "mail")
public class Mail {

    @Id(value = "auto")
    private Integer id;

    private String name;

    private String status = "0";
}
