package com.eazy.lksy.web.core.dao;

import java.io.Serializable;
import java.util.List;

public interface CommonService<T>  {

	public List<T> select();
	
	public boolean insert(T entity);

	public T selectById(Serializable id);

	public boolean deleteById(Serializable id);

	public boolean update(T entity);

	public int count();
}
