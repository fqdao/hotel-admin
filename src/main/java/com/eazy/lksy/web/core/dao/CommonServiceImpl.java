package com.eazy.lksy.web.core.dao;

import java.io.Serializable;
import java.util.List;

public class CommonServiceImpl<T>  extends CommonDaoImpl<T> implements CommonService<T> {


	@Override
	public List<T> select() {
		return super.select();
	}

	@Override
	public boolean insert(T entity) {
		return super.insert(entity);
	}

	@Override
	public T selectById(Serializable id) {
		return super.selectById(id);
	}

	@Override
	public boolean deleteById(Serializable id) {
		return super.deleteById(id);
	}

	@Override
	public boolean update(T entity) {
		return super.update(entity);
	}

	@Override
	public int count() {
		return super.count();
	}
}
