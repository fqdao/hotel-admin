package com.eazy.lksy.web.dao;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.web.core.dao.BaseDao;

import java.util.List;
import java.util.Map;

@Repository
public class HotelDao  extends BaseDao {

    public List<Map<String, Object>> getRoomImage(String hotelId) {
        return dao.queryForList("SELECT ri.*,r.`name` FROM room r,room_image ri WHERE r.`id` = ri.`r_id`  AND r.`h_id`=" + hotelId);
    }

//	public void addHotel(DbEntity dbEntity) {
//		dao.update("insert into hotel(hotel_name,tel,address,img,star) values(?,?,?,?,?)",dbEntity.get("hotel_name"),dbEntity.get("hotel_tel"),dbEntity.get("hotel_addr"),dbEntity.get("photo_name"),dbEntity.get("hotel_star"));
//	}
}
