package com.eazy.lksy.web.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.eazy.lksy.web.view.PageList;
import com.eazy.lksy.web.view.vo.ResponseUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.eazy.lksy.web.core.dao.BaseDao;
import com.eazy.lksy.web.model.User;

@Repository
public class UserDao extends BaseDao {

	public User getUserByName(String userName) {
		String sql = "select * from user where name=? ";
		RowMapper<User> rm = ParameterizedBeanPropertyRowMapper.newInstance(User.class);
		return  queryForObject(sql, new Object[] { userName}, rm);
	}

	public void updateVisit(Integer userId) {
		dao.update("update user set login_count=login_count+1,last_visit=? where id=?",new Object[]{new Date(),userId});
	}

	public void addUserRole(String cust_id,String role_id) {
		String sql = "insert into user_role(USER_ID,ROLE_ID) values(?,?)";
		dao.update(sql,cust_id,role_id);
	}

    public PageList getUserPage(Integer start, Integer length) {
		return  queryByPageForMySQL("select * from user",null,start,length, ResponseUser.class);
    }
}