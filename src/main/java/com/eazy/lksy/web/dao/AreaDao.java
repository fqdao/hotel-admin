package com.eazy.lksy.web.dao;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.eazy.lksy.web.core.dao.BaseDao;

@Repository
public class AreaDao extends BaseDao {

	public List<Map<String, Object>> list(String cityId) {
		return dao.queryForList("select * from area where city_id=?",cityId);
	}

}
