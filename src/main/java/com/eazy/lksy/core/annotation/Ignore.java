package com.eazy.lksy.core.annotation;

import java.lang.annotation.*;

/**
 * 此注解标注的字段作为业务逻辑字段，不参与数据库持久化
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Ignore {

}
