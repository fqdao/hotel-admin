package com.eazy.lksy.core.annotation;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Date {

    //如果默认用auto走数据库默认时间
    String type() default "auto";

    String format() default "yyyy-MM-dd HH:ss:mm";
}
