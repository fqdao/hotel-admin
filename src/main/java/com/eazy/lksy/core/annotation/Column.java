package com.eazy.lksy.core.annotation;

import java.lang.annotation.*;

/**
 * 数据库对应字段
 * 例子：
 *  java 字段 cityId 数据库 city_Id
 * @Column(field = "city_id")
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Column {

    String field() default "";
}
